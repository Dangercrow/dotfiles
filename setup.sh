#!/usr/bin/env sh
# Gitpod runs us with CWD != Script location, so we must work around this

SCRIPT=$(readlink -f "$0")
SCRIPT_PATH=$(dirname "${SCRIPT}")

${SCRIPT_PATH}/git.sh