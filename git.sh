#!/usr/bin/env sh
git config --global core.editor nano

git config --global fetch.prune true

git config --global alias.co checkout
git config --global alias.st status
git config --global alias.ci commit
git config --global alias.cp cherry-pick
git config --global alias.om '!git fetch && git co origin/HEAD && :'
git config --global alias.pb '!git push origin HEAD:refs/heads/$1 && :'
git config --global alias.pbf '!git push origin HEAD:refs/heads/$1 --force-with-lease && :'
git config --global alias.amend 'ci -a --no-edit --amend'
git config --global alias.br 'branch -vv'
git config --global alias.frb '!git fetch && git fetch . origin/$1/$1 && git rebase -i origin/HEAD $1 && :'
